/* eslint-disable react/prop-types */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/button-has-type */
/* eslint-disable react/jsx-filename-extension */
/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import '../App.global.css';

class Button extends Component {
  render() {
    return (
      <button className="App-Button" onClick={this.props.onClick}>
        {this.props.label}
      </button>
    );
  }
}
export default Button;
