/* eslint-disable react/jsx-filename-extension */
/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import '../App.global.css';

class Loader extends Component {
  render() {
    return (
      <div>
        <image
          className="loader"
          alt="Loader"
          width="30"
          height="30"
          src="../img/ZZ5H.gif"
        />
      </div>
    );
  }
}
export default Loader;
