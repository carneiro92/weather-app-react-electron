/* eslint-disable react/prop-types */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/button-has-type */
/* eslint-disable react/jsx-filename-extension */
/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import '../App.global.css';
import ForecastChooseCity from './ForecastChooseCity';
import ForecastForm from './ForecastForm';
import ForecastResult from './ForecastResult';
import ForecastTitle from './ForecastTitle';

class Forecast extends Component {
  render() {
    return (
      <div className="forecast">
        <ForecastChooseCity />
        <ForecastTitle />
        <ForecastResult />
        <ForecastForm />
      </div>
    );
  }
}
export default Forecast;
