/* eslint-disable react/prop-types */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/button-has-type */
/* eslint-disable react/jsx-filename-extension */
/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import '../App.global.css';

class ForecastChooseCity extends Component {
  render() {
    return <h1 className="title">{this.props.forecast.location.name}</h1>;
  }
}
const mapStateToProps = (state) => {
  return {
    forecast: state.weatherReducer.forecast,
  };
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(ForecastChooseCity);
