/* eslint-disable react/prop-types */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/button-has-type */
/* eslint-disable react/jsx-filename-extension */
/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import '../App.global.css';

class ForecastTitle extends Component {
  render() {
    const date = Date(this.props.forecast.location.localtime);
    const treatedDate = date.replace(
      'GMT+0100 (heure normale d’Europe centrale)',
      ''
    );
    return (
      <div>
        <p>{treatedDate}</p>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    forecast: state.weatherReducer.forecast,
  };
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(ForecastTitle);
