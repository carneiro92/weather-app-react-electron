const INITIAL_STATE = {
  forecast: {
    location:{name:null}, 
    current:{temperature:null,weather_icons:[null], weather_descriptions:[null]}, 
  },
  loader: false,
  input: "Bordeaux"
};
function weatherReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case "UPDATE_INPUT":
      return {
        ...state,
        input: action.value,
      };
    case "UPDATE_LOADER":
      return {
        ...state,
        loader: action.status,
      };
    case "UPDATE_FORECAST":
      return {
        ...state,
        forecast: action.value,
      };
    default:
      return state;
  }
}
export default weatherReducer;
